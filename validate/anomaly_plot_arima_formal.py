"""Analyze the anomaly scores, compute performance metrics and generate figures"""

import pickle
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import (
    accuracy_score,
    precision_recall_fscore_support,
    roc_auc_score,
)
from datetime import datetime, timedelta
import csv

import copy


# in "anomaly_plot_arima_new.py", we use the gt label to discriminate the type 1 or 2 loss, which fixed the bug before. Though the results are not so different


def difference_time_A_B_seconds(timeA, timeB, format_pattern="%m/%d/%Y %H:%M"):

    gap_seconds = (
        datetime.strptime(timeA, format_pattern)
        - datetime.strptime(timeB, format_pattern)
    ).total_seconds()

    return gap_seconds


def process_partial_loss_seq(all_period_loss, time, data, target, outcome, ind):

    ALL_LOSS_ = []
    TIME_ = []
    IS_COVID_ADM_ = []

    for i, sample in enumerate(data):

        sample_period_loss = np.array(all_period_loss[i])[:, ind].tolist()

        sample_dic = sample["df_sample_ind_ori"]
        seq_len = sample["seq_len"]

        GT = sample_dic[outcome].isin(target).values

        if (
            np.sum(sample_dic[outcome].isin(target)[1:]) == 0
        ):  # -1: no count for the first period since no GT
            continue

        sample_period_loss_valid = [
            sample_period_loss[j] for j in range(seq_len - 1) if GT[j + 1] == True
        ]

        if sample_period_loss_valid == []:
            raise ValueError("sample_period_loss_valid empty")

        ALL_LOSS_.append(np.mean(sample_period_loss_valid))
        TIME_.append(time[i])
        IS_COVID_ADM_.append(sample["is_covid_adm"])

    return ALL_LOSS_, TIME_, IS_COVID_ADM_


plt.figure(figsize=(10, 100))


feature_list = [
    (6, "ICU"),
    (7, "intu"),
    (-1, "seq death"),
]


DATA = pickle.load(open("BMC.pkl", "rb"))

print("DATA loaded")


out = open("performance_record.csv", "a", newline="", encoding="utf-8")
csv_write = csv.writer(out, dialect="excel")


model_name_list = [
    "all_loss_ep5_IgnoreIndex_-100.pkl",
    "all_loss_ep10_IgnoreIndex_-100.pkl",
    "all_loss_ep15_IgnoreIndex_-100.pkl",
    "all_loss_ep20_IgnoreIndex_-100.pkl",
    "all_loss_ep25_IgnoreIndex_-100.pkl",
    "all_loss_ep30_IgnoreIndex_-100.pkl",
    "all_loss_ep35_IgnoreIndex_-100.pkl",
    "all_loss_ep40_IgnoreIndex_-100.pkl",
    "all_loss_ep45_IgnoreIndex_-100.pkl",
    "all_loss_ep50_IgnoreIndex_-100.pkl",
    "all_loss_ep55_IgnoreIndex_-100.pkl",
    "all_loss_ep60_IgnoreIndex_-100.pkl",
    "all_loss_ep65_IgnoreIndex_-100.pkl",
    "all_loss_ep70_IgnoreIndex_-100.pkl",
    "all_loss_ep75_IgnoreIndex_-100.pkl",
    "all_loss_ep80_IgnoreIndex_-100.pkl",
    "all_loss_ep85_IgnoreIndex_-100.pkl",
    "all_loss_ep90_IgnoreIndex_-100.pkl",
    "all_loss_ep95_IgnoreIndex_-100.pkl",
    "all_loss_ep100_IgnoreIndex_-100.pkl",
    "all_loss_ep105_IgnoreIndex_-100.pkl",
    "all_loss_ep110_IgnoreIndex_-100.pkl",
    "all_loss_ep115_IgnoreIndex_-100.pkl",
    "all_loss_ep120_IgnoreIndex_-100.pkl",
    "all_loss_ep125_IgnoreIndex_-100.pkl",
    "all_loss_ep130_IgnoreIndex_-100.pkl",
    "all_loss_ep135_IgnoreIndex_-100.pkl",
    "all_loss_ep140_IgnoreIndex_-100.pkl",
    "all_loss_ep145_IgnoreIndex_-100.pkl",
    "all_loss_ep150_IgnoreIndex_-100.pkl",
]


target_list = [[0], [1], [0, 1]]


for model_name in model_name_list:

    print("Now checking model: " + model_name)

    for target in target_list:

        out_line = [model_name, target]

        for o, (feature_ind, feature_name) in enumerate(feature_list):

            [ALL_LOSS, ALL_PERIOD_LOSS, TIME, SEQ_LEN, IS_COVID_ADM] = pickle.load(
                open(model_name, "rb")
            )
            target = target  # type 1 loss

            if feature_name == "ICU":

                ALL_LOSS, TIME, IS_COVID_ADM = process_partial_loss_seq(
                    all_period_loss=ALL_PERIOD_LOSS,
                    time=TIME,
                    data=DATA,
                    target=target,
                    outcome="icu_records",
                    ind=feature_ind,
                )

            if feature_name == "intu":

                ALL_LOSS, TIME, IS_COVID_ADM = process_partial_loss_seq(
                    all_period_loss=ALL_PERIOD_LOSS,
                    time=TIME,
                    data=DATA,
                    target=target,
                    outcome="intu_records",
                    ind=feature_ind,
                )

            if feature_name == "seq death":

                ALL_LOSS = np.array(ALL_LOSS)[:, feature_ind].tolist()

                TIME = [
                    TIME[i]
                    for i in range(len(TIME))
                    if DATA[i]["eventually_die_within3weeks_IncludingHome"] in target
                ]
                IS_COVID_ADM = [
                    IS_COVID_ADM[i]
                    for i in range(len(IS_COVID_ADM))
                    if DATA[i]["eventually_die_within3weeks_IncludingHome"] in target
                ]
                ALL_LOSS = [
                    ALL_LOSS[i]
                    for i in range(len(ALL_LOSS))
                    if DATA[i]["eventually_die_within3weeks_IncludingHome"] in target
                ]

            DATE = [i.split()[0] for i in TIME]

            test_time = "01/01/2019 00:00"  # initial date for testing

            daily_patient_mean_loss_dic = {}
            daily_patient_covid_label_dic = {}

            for i, day in enumerate(TIME):

                day_gap = int(
                    difference_time_A_B_seconds(
                        timeA=day, timeB=test_time, format_pattern="%m/%d/%Y %H:%M"
                    )
                    / (3600 * 24 * 1)
                )

                if day_gap not in daily_patient_mean_loss_dic:
                    daily_patient_mean_loss_dic[day_gap] = []
                    daily_patient_covid_label_dic[day_gap] = []

                daily_patient_mean_loss_dic[day_gap].append(ALL_LOSS[i])
                daily_patient_covid_label_dic[day_gap].append(IS_COVID_ADM[i])

            for day in daily_patient_mean_loss_dic:

                daily_patient_mean_loss_dic[day] = np.mean(
                    daily_patient_mean_loss_dic[day]
                )
                if np.sum(daily_patient_covid_label_dic[day]) > 0:
                    daily_patient_covid_label_dic[day] = 1
                else:
                    daily_patient_covid_label_dic[day] = 0

            tuple_list = [
                (k, daily_patient_mean_loss_dic[k]) for k in daily_patient_mean_loss_dic
            ]

            tuple_list.sort(key=lambda a: a[0])

            day_axis = [i[0] for i in tuple_list]
            loss_axis = [i[1] for i in tuple_list]

            # ---------------------- compute covid detection auc using the is_covid_adm

            weekly_test_loss_cases = [
                daily_patient_mean_loss_dic[day]
                for day in daily_patient_mean_loss_dic.keys()
                if day > 0 and day < 500
            ]
            weekly_test_covid_indicator = [
                daily_patient_covid_label_dic[day]
                for day in daily_patient_mean_loss_dic.keys()
                if day > 0 and day < 500
            ]

            print(len(weekly_test_loss_cases))
            print(len(weekly_test_covid_indicator))
            print("total covid adms:", np.sum(weekly_test_covid_indicator))

            weekly_covid_auc = roc_auc_score(
                weekly_test_covid_indicator, weekly_test_loss_cases
            )

            print(
                feature_name + " unsupervised anomaly detection AUC (weekly):",
                weekly_covid_auc,
            )

            out_line.extend(["weekly " + feature_name, weekly_covid_auc])

            test_loss_cases = [
                ALL_LOSS[i]
                for i in range(len(ALL_LOSS))
                if difference_time_A_B_seconds(
                    timeA=TIME[i], timeB=test_time, format_pattern="%m/%d/%Y %H:%M"
                )
                > 0
                and difference_time_A_B_seconds(
                    timeA=TIME[i], timeB=test_time, format_pattern="%m/%d/%Y %H:%M"
                )
                < 500 * 24 * 3600
            ]
            test_covid_indicator = [
                IS_COVID_ADM[i]
                for i in range(len(IS_COVID_ADM))
                if difference_time_A_B_seconds(
                    timeA=TIME[i], timeB=test_time, format_pattern="%m/%d/%Y %H:%M"
                )
                > 0
                and difference_time_A_B_seconds(
                    timeA=TIME[i], timeB=test_time, format_pattern="%m/%d/%Y %H:%M"
                )
                < 500 * 24 * 3600
            ]

            covid_auc = roc_auc_score(test_covid_indicator, test_loss_cases)

            print(feature_name + " unsupervised anomaly detection AUC:", covid_auc)

            out_line.extend([feature_name, covid_auc])

            # ---------------------- compute arima residuals

            train_loss_for_arima = [
                loss_axis[i] for i in range(len(loss_axis)) if day_axis[i] < 0
            ]

            import numpy as np
            import pandas as pd
            from statsmodels.tsa.arima.model import ARIMA
            import matplotlib.pyplot as plt
            from scipy.stats import norm

            model = ARIMA(train_loss_for_arima, order=(5, 1, 0))
            model_fit = model.fit()

            new_model_fit = model_fit.apply(
                loss_axis, refit=False
            )  # use the trained arima model to predict the whole dataset, without fitting the test set

            res_axis = new_model_fit.resid
            res_axis = abs(res_axis).tolist()

            pred_axis = new_model_fit.fittedvalues.tolist()

            # ----------------------

            train_loss = [
                loss_axis[i] for i in range(len(loss_axis)) if day_axis[i] < 0
            ]
            train_res = [
                res_axis[i] for i in range(len(res_axis)) if day_axis[i] < 0
            ]  # now use the arima res to compute 3 sigma

            train_loss_mean = np.mean(
                train_loss
            )  # skip the first several points in arima to avoid outliers
            train_loss_std = np.std(train_loss)

            train_res_mean = np.mean(
                train_res[20:]
            )  # skip the first several points in arima to avoid outliers
            train_res_std = np.std(train_res[20:])

            threshold = 4
            plt.subplot(int(float(str(len(feature_list)) + "1" + str(o + 1))))

            plt.plot(day_axis, loss_axis, color="blue", label=r"loss")
            plt.plot(day_axis, pred_axis, color="green", label=r"arima_pred")
            plt.plot(
                day_axis,
                [train_loss_mean] * len(day_axis),
                color="red",
                linestyle="-.",
                label=r"train_loss_mean",
            )
            plt.plot(
                day_axis,
                [train_loss_mean + threshold * train_loss_std] * len(day_axis),
                color="red",
                label="train_loss_mean + " + str(threshold) + " sigma",
            )

            plt.scatter(
                [
                    day_axis[i]
                    for i in range(len(day_axis))
                    if loss_axis[i] > train_loss_mean + threshold * train_loss_std
                ],
                [
                    loss_axis[i]
                    for i in range(len(loss_axis))
                    if loss_axis[i] > train_loss_mean + threshold * train_loss_std
                ],
                s=40,
                c="red",
                marker="x",
            )

            plt.scatter(
                [
                    day_axis[i]
                    for i in range(len(day_axis))
                    if res_axis[i] > train_res_mean + threshold * train_res_std
                ],
                [
                    loss_axis[i]
                    for i in range(len(loss_axis))
                    if res_axis[i] > train_res_mean + threshold * train_res_std
                ],
                s=20,
                c="orange",
                marker="D",
            )

            plt.xlabel("week")
            plt.ylabel(feature_name + " loss")

            plt.grid(True)
            plt.legend()

        csv_write.writerow(
            out_line
        )  # record all outcomes' AUCs for covid detection under each (model, target) combination


out.close()
